# signal-klieh-rest-api
## Installation
First install docker and docker-compose, then:
```
git glone https://gitlab.com/mo_krauti/signal-klieh-rest-api.git
cd signal-klieh-rest-api/build && ./build.sh
```

## Configuration
Start up the container for the first time:
```
docker-compose up -d
```
Then start a shell in the container and set up signal-cli by [registering](https://github.com/AsamK/signal-cli#usage) a new signal account or [linking](https://github.com/AsamK/signal-cli/wiki/Linking-other-devices-(Provisioning)) to another device.
```
docker exec -it signal-api bash
# register or link using signal-cli
exit
docker-compose down
```
Edit build/code/server.py and set NUM variable to your phone number. Rebuild the container.

## Usage
Start container:
```
docker-compose up -d
```

List all Messages:
```
curl -i localhost:4321/msg
```
List all Messages since unix_timestamp:
```
curl -i localhost:4321/msg/new/unix_timestamp
```
List all Messages from group / chat:
```
curl -i localhost:4321/msg/PHONE_NUMBER_OR_GROUP_BASE64_STRING
```
Send Message:
```
curl -i -H "Content-Type: application/json" -X POST -d '{"target":"PHONE_NUMBER_OR_GROUP_BASE64_STRING", "text":"Hello World!"}' localhost:4321/send
```
