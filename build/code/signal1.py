from threading import Thread
import base64
import time
from server import db, Message, NUM

#NUM = '+49175825589'

def onMsgRcv(timestamp, source, groupID, text, attachments):
    timestamp = int(str(timestamp)[:-3])
    groupID = base64.b64encode(bytearray(groupID)).decode()
    if groupID == '':
        target = NUM
    else:
        target = groupID
    #print(timestamp, source, target, text)
    msg = Message(timestamp=timestamp, source=source, target=target, text=text)
    db.session.add(msg)
    db.session.commit()

def sendMsg(target, text):
    timestamp = int(time.time())
    if target[0] == '+': #private chat
        signal.sendMessage(text, [], [target])
    else:
        group = list(base64.b64decode(target))
        signal.sendGroupMessage(text, [], group)
    msg = Message(timestamp=timestamp, source=NUM, target=target, text=text)
    db.session.add(msg)
    db.session.commit()

def recvLoop():
    print('Running recv loop')
    loop.run()

from pydbus import SystemBus
from gi.repository import GLib

bus = SystemBus()
loop = GLib.MainLoop()
signal = bus.get('org.asamk.Signal')
signal.onMessageReceived = onMsgRcv
recvThread = Thread(target=recvLoop, daemon=True)
recvThread.start()

