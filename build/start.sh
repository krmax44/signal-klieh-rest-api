dbus-daemon --nopidfile --system &
[ "$(ls -A /root/.local/share/signal-cli)" ] && echo "Starting signal-cli dbus-daemon ..." || echo "Doing nothing, start shell in container and set up signal-cli!" && while true; do sleep 100; done
signal-cli -u +491758255898 daemon --system &
echo 'Waiting 2 min for signal-cli dbus-daemon startup...'
sleep 120
python3 /root/code/server.py
